#!/bin/sh

set -ue

LOGFILE=/tmp/irobot-syslog.log

function do_log() {
	timestamp=`date '+%Y-%m-%d_%H:%M:%S'`

	# Top 10 processes sort by CPU usage.
	# NOTE: only pids are logged, more process information could be retrieved
	# from `ps` if required.
	top_processes=`ps -eo pid --sort=-pcpu | tail -n +2 | head -n 10 | tr '\n' ' '`

	# Average system load for the past minute.
	# Alternatives: mpstat, top.
	system_load=`cat /proc/loadavg | awk '{ printf $1 }'`

	# Available memory in kB.
	available_memory=`awk '/MemAvailable/ { printf $2 }' /proc/meminfo`

	# Use one line per log entry, which includes timestamp header and
	# all measurements.
	echo "$timestamp $top_processes $system_load $available_memory"
}

rm -f "$LOGFILE"
echo -n "" > "$LOGFILE"

while : ; do
	echo "Running irobot-syslog-tmp.sh"
	# Avoid file locking by using a RCU like mechanism.
	# A temporary copy is made in the same tmpfs and then modified.
	# Calling mv on the same fs will result in a call to rename(),
	# which is atomic.
	logfile_tmp="$LOGFILE.tmp"
	cp "$LOGFILE" "$logfile_tmp"
	do_log >> "$logfile_tmp"
	mv "$logfile_tmp" "$LOGFILE"
	sleep 10
done
