#!/bin/sh

set -ue

SRCFILE=/tmp/irobot-syslog.log
DESTPATH=/root

function do_log() {
	if [ ! -f "$SRCFILE" ]; then
		return
	fi
	timestamp=`systemctl show irobot-syslog-tmp.service -p ExecMainStartTimestamp --value | cut -d' ' -f2-`
	destfile="$DESTPATH/irobot-syslog_$timestamp.log"
	destfile_tmp="$destfile.tmp"
	# Unclean reboots could lead to corruption or inconsistency in the
	# destination file. To avoid this, the original file is copied to the
	# destination fs. Then renamed using the mv utility, which is atomic.
	# See: http://www.linux-mtd.infradead.org/faq/ubifs.html#L_atomic_change
	dd if="$SRCFILE" of="$destfile_tmp" conv=fsync
	mv "$destfile_tmp" "$destfile"
}

while : ; do
	echo "Running irobot-syslog-persist.sh"
	do_log
	sleep $((10 * 60))
done
