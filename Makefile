# NOTE: Systemd System Unit Path could be detected from pkgconfig or
# manually set from environment. For simplicity, let's just use the
# default path here.
# For portability use Autotools or Meson build systems.

BINDIR ?= /usr/bin
SYSTEMDUNITDIR ?= /usr/lib/systemd/system

install:
	install -D -m 755 irobot-syslog-tmp.sh $(BINDIR)/irobot-syslog-tmp.sh
	install -D -m 644 irobot-syslog-tmp.service $(SYSTEMDUNITDIR)/irobot-syslog-tmp.service
	install -D -m 755 irobot-syslog-persist.sh $(BINDIR)/irobot-syslog-persist.sh
	install -D -m 644 irobot-syslog-persist.service $(SYSTEMDUNITDIR)/irobot-syslog-persist.service
